module.exports = function( grunt ){
    var banner = `( function( root, factory ){
if( typeof define === "function" && define.amd ){
    define( [ "exports", "underscore", "backbone" ], factory );
}
else if( typeof exports === "object" && typeof exports.nodeName !== "string" ){
    factory( exports, require("underscore"), require("backbone") );
}
else{
    factory( window, window._, window.Backbone );
}
}( window, function( exports, _, Backbone ){`;
    var footer = `exports.Ribcage = Layout;
} ) );`;

    return {
        "ribcage": {
            "options": {
                "banner": banner,
                "footer": footer
            },
            "src": [ "src/region.js", "src/layout.js" ],
            "dest": "build/ribcage.js"
        }
    };
};
