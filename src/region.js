var Region = function( parent, selector ){
    this.output = parent.find( selector );
    this.view = undefined;

    this._isRendered = false;
    this._renderable = undefined;
    this._viewData = {};

    this.hasRegions = function(){
        return this.view && _( this.view ).has( "regions" );
    };
};

Region.prototype.remove = function(){
    if( this._isRendered ){
        if( this.hasRegions() ){
            this.view.remove();
        }
        else{
            this.view.stopListening();
            this.view.undelegateEvents();
            this.view.$el.empty();
        }

        this._isRendered = false;
    }
};

Region.prototype.render = function(){
    var self = this;
    var layout;

    this.remove();


    if( typeof this._renderable === 'function' ){
        this._renderable.prototype.el = this.output;
        this._renderable.prototype.getRegion = function(){
            return self;
        };

        this.view = new this._renderable( this._viewData );
    }
    else if( typeof this._renderable === 'object' ){
        this._renderable.el = this.output;
        this.view = this._renderable;
    }

    this._isRendered = true;

    if( this.hasRegions() ){
        layout = this.view.render();

        this.regions = layout.regions;
    }
};

Region.prototype.show = function( renderable, optionalViewData ){
    this._renderable = renderable;
    this._viewData = optionalViewData;

    this.render();
};
