var secrets = {};

var Layout = function( config ){
    secrets.config = config;

    this.isLive = false;

    this.extract();
};

Layout.prototype.getConfig = function(){
    return secrets.config;
};

Layout.prototype.extract = function(){
    var conf = this.getConfig();
    var regions = {};
    var presets = {};

    _( conf.regions ).each( function( selector, name ){
        regions[ name ] = selector;
    } );

    _( conf.presets ).each( function( preset, name ){
        presets[ name ] = preset;
    } );

    this.el = conf.element;
    this.tmpl = conf.template;
    this.regions = regions;
    this.presets = presets;
};

Layout.prototype.render = function(){
    var self = this;
    var view = this.createView( this.el, this.tmpl, this.regions );

    this.live = new view();

    _( this.presets ).each( function( renderable, name ){
        self.regions[ name ].show( renderable );
    });

    this.isLive = true;

    return this;
};

Layout.prototype.remove = function(){
    _( this.regions ).each( function( region ){
        region.remove();
    });
};

Layout.prototype.explore = function( path ){
    if( !this.isLive ){
        throw new Error( "The layout must be rendered in order to explore regions." );
    }
    else{
        var steps = path.split( /[\.:]/ );
        var region = this;
        var count = 0;
        var name = "this layout";

        _( steps ).each( function( step ){
            if( _( region ).has( "regions" ) ){
                if( _( region.regions ).has( step ) ){
                    region = region.regions[ step ];
                }
                else{
                    throw new Error( '"' + step + '" is not a region inside "' + name + '".' );
                }
            }
            else if( _( steps ).size() !== count ){
                throw new Error( '"' + name + '" does not contain regions.' );
            }

            count++;
            name = step;
        });

        return region;
    }
};

Layout.prototype.createView = function( el, tmpl, regions ){
    var self = this;

    return Backbone.View.extend({
        "el": el,

        "render": function(){
            this.$el.html( tmpl );
            self.createRegions( regions, this.$el );

            return this;
        },

        "initialize": function(){
            this.render();
        }
    });
};

Layout.prototype.createRegions = function( regions, parent ){
    var self = this;

    _( regions ).each( function( selector, name ){
        self.regions[ name ] = new Region( parent, selector );
    });
};
